FROM python:3.7

WORKDIR /usr/src/djangoapp

COPY app/requirements.txt ./
RUN pip install -r requirements.txt

COPY ./app ./
EXPOSE 8000
CMD ["gunicorn", "-c", "gunicorn.py", "--chdir" , "/usr/src/djangoapp/app", "app.wsgi"]