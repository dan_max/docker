from django.http import HttpResponse
from django.shortcuts import render

import psycopg2


def list_page(request):
    conn = psycopg2.connect(dbname="postgres", user="postgres", password="0126", host="db")
    cur = conn.cursor()
    cur.execute("SELECT * FROM list WHERE done = 'f';")
    data = cur.fetchall()
    s = "<ul>"
    for i in data:
        s += "<li"
        s += " id="
        s += str(i[0])
        s += ">"
        s += i[1]
        s += "</li>\n"
    s += "</ul>\n"
    cur.execute("SELECT * FROM list WHERE done = 't';")
    data = cur.fetchall()
    s2 = "<ul hidden=true id=closed>"
    for i in data:
        s2 += "<li"
        s2 += " id="
        s2 += str(i[0])
        s2 += ">"
        s2 += i[1]
        s2 += "\t"
        s2 += "date: "
        s2 += str(i[3])
        s2 += "</li>\n"
    s2 += "</ul>\n"
    return HttpResponse("""<head>
        <script type="text/javascript" src="/static/js/tmp.js"></script>
        </head> 
        <body>
        <h1>the list page\n</h1>
        open task  %s
        closed task <button id=sw onclick=swt()>show</button> %s 
        </body>""" % (s, s2))

def index(request):
    return HttpResponse("""<h1>this is a start page\n</h1>
    <a href=\"/list/\"> list</a>""")
